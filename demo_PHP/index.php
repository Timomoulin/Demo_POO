
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo POO</title>
</head>
<body>
    <h1>Demo POO</h1>
    <?php
    require 'classes/utilisateur.class.php';
    $moi= new Utilisateur("Moulin","Timothée","timomoulin@msn.com");
    echo $moi->affiche();
    $utilisateur1= new Utilisateur("Bidule","Marcel","marcelbidule@gmail.com");
    echo $utilisateur1->affiche();
    echo "<br> ".Utilisateur::msg();
    ?>
</body>
</html>


