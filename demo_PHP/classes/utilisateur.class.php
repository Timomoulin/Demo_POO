<?php
class Utilisateur //nom de la classe commence avec une majuscule
{ //Debut de la classe

//---Les attributs
public $nom; //un attribut nom 
private $prenom; //un attribut prenom
private $email; //un attribut email
//---Fin des attributs

//---Les méthodes
public function __construct($unNom,$unPrenom,$unEmail)
{
    $this->nom=$unNom;
    $this->prenom=$unPrenom;
    $this->email=$unEmail;

}
public function getNom()
{
    return $this->nom ;
    
}

public function getPrenom()
{
    return $this->Prenom ;
}

public function getEmail()
{
    return $this->Email;
}

public function setNom($unNom)
{
    $this->nom = $unNom ;
}

public function setPrenom($unPrenom)
{
    $this->prenom = $unPrenom ;
}

public function affiche()
{
    return "<br> nom : <b>".$this->nom."</b> <br> prenom : <b>". $this->prenom. "</b> <br> email : <b>". $this->email."</b> <br>";
}
public static function msg()
{
    return "un msg statique";
}
//---Fin des méthodes

} //Fin de la classe
?>